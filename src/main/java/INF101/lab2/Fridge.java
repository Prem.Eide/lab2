package INF101.lab2;

import java.util.ArrayList;
import java.util.List;

import java.util.NoSuchElementException;

public class Fridge implements IFridge {

    ArrayList<FridgeItem> content = new ArrayList<>();
    int maxCapacity = 20;

    public int nItemsInFridge() {
        return content.size();
    }

    public int totalSize() {
        return maxCapacity;
    }

    public boolean placeIn(FridgeItem item) {
        if (nItemsInFridge() == totalSize()) {
            return false;
        }
        content.add(item);
        return true;
    }

    public void takeOut(FridgeItem item) {
        if (content.contains(item)) {
            content.remove(item);
        } else {
            throw new NoSuchElementException();
        }
    }

    public void emptyFridge() {
        content.clear();
    }

    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expiredFood = new ArrayList<>();
        for (FridgeItem item : content) {
            if (item.hasExpired()) {
                expiredFood.add(item);
            }
        }
        for (FridgeItem item : expiredFood) {
            takeOut(item);
        }
        return expiredFood;
    }

}